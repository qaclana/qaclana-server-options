image:https://gitlab.com/qaclana/qaclana-server-options/badges/master/build.svg["Build Status", link="https://gitlab.com/qaclana/qaclana-server-options/commits/master"]

= Qaclana Server Options

This is a library for server modules to load properties based on the command line options.

== How to build Qaclana API from source

To generate a JAR, run:
[source,bash]
----
$ ./gradlew build
----

== Releasing
=== Automated release
For every commit that reaches master, a new version is generated and uploaded to OSSRH, which
publishes to Maven Central.

=== Manual release
To release, you'll need a `~/.gradle/gradle.properties`, with the following properties:

```
signing.keyId=key-id
signing.password=secret-key-passphrase
signing.secretKeyRingFile=/home/your-username/.gnupg/secring.gpg

ossrhUsername=your-maven-central-username
ossrhPassword=your-maven-central-password
```

With that, and given that you have the proper permissions, just run:
[source,bash]
----
$ ./gradlew upload
----

== License

Qaclana is released under Apache License, Version 2.0 as described in the link:LICENSE[LICENSE] document

----
   Copyright 2016-2017 Juraci Paixão Kröhling

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
----
