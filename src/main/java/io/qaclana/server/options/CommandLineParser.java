/*
 * Copyright 2017 Juraci Paixão Kröhling
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qaclana.server.options;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import io.qaclana.api.Configuration;

/**
 * @author Juraci Paixão Kröhling
 */
public class CommandLineParser {
    private static final Logger logger = Logger.getLogger(CommandLineParser.class.getName());
    private static CommandLineParser INSTANCE;

    private String[] parameters;
    private CommandLine cmd;

    private CommandLineParser(String[] parameters) {
        this.parameters = parameters;
        prepareCommandLineOptions();
    }

    public static CommandLineParser build(String[] parameters) {
        INSTANCE = new CommandLineParser(parameters);
        return INSTANCE;
    }

    private void prepareCommandLineOptions() {
        Options options = new Options();
        options.addOption("b", "bind", true, "IP address to bind to");
        options.addOption("p", "port", true, "Port to bind to");
        options.addOption("c", "conf", true, "Configuration file to use");
        options.addOption("t", "target", true, "URL for the Target server, like http://ip:port/path");
        options.addOption("hb", "healthcheck-bind", true, "IP address to bind the health check service to");
        options.addOption("hp", "healthcheck-port", true, "Port to bind the health check service to");

        try {
            cmd = new DefaultParser().parse(options, parameters, false);
        } catch (ParseException e) {
            throw new RuntimeException("Could not parse configuration file", e);
        }

        String configurationFilePath = "conf/configuration.properties";
        if (cmd.hasOption("c")) {
            configurationFilePath = cmd.getOptionValue("c");
        }
        String logMessage = String.format("Configuration file to use: [%s]", configurationFilePath);
        logger.finest(() -> logMessage);

        if (!Files.exists(Paths.get(configurationFilePath))) {
            throw new IllegalStateException(String.format("The configuration file [%s] does not exist.", configurationFilePath));
        }

        try {
            System.getProperties().load(new FileReader(new File(configurationFilePath)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (cmd.hasOption("p")) {
            System.setProperty(Configuration.PORT, cmd.getOptionValue("p"));
        }

        if (cmd.hasOption("t")) {
            System.setProperty(Configuration.TARGET, cmd.getOptionValue("t"));
        }

        if (cmd.hasOption("hb")) {
            System.setProperty(Configuration.HEALTHCHECK_BIND, cmd.getOptionValue("hb"));
        }

        if (cmd.hasOption("hp")) {
            System.setProperty(Configuration.HEALTHCHECK_PORT, cmd.getOptionValue("hp"));
        }
    }
}
